$submodules = Get-ChildItem .\submodules

foreach ($s in $submodules) {
    pushd 
    try {
       cd "$($s.fullname)"
     
        if (test-path ".\scripts\test.appveyor.ps1") {
            & ".\scripts\test.appveyor.ps1"
        } else {
            & ".\scripts\test.ps1"    
        }
    } finally {
        popd
    }
}