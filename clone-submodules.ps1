# these env variables should be private:
$privkey = $env:priv_key
$repo = $env:repo

if ($repo.endswith(".hg")) {
    $vsc = "hg"
} elseif($repo.endswith(".git")){
    $vcs = "git"
}


write-host "testing if ssh is available"
get-command "ssh.exe"

#use ssh from git
'[ui]' | out-file  "$env:USERPROFILE/mercurial.ini" -Append -Encoding utf8
'ssh=ssh.exe' | out-file "$env:USERPROFILE/mercurial.ini" -Append -Encoding utf8

$bbhostkey = @"
bitbucket.org,104.192.143.3 ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==
"@


write-host "adding bitbucket to known_hosts"
$bbhostkey | out-file "$env:USERPROFILE/.ssh/known_hosts" -Append -Encoding utf8

write-host "adding private key"
$fileContent = "-----BEGIN RSA PRIVATE KEY-----`n"
$fileContent += $privkey.Replace(' ', "`n")
$fileContent += "`n-----END RSA PRIVATE KEY-----`n"
Set-Content "$env:USERPROFILE\.ssh\id_rsa" $fileContent


if ($vcs -eq "hg") {    
    write-host "testing  ssh"
    ssh hg@bitbucket.org "hg -R $repo"
}

if (!(test-path "submodules")) {
    $null = new-item -type directory "submodules"
}

pushd 
try {
    cd "submodules"
    $branch = $env:APPVEYOR_REPO_BRANCH
    write-host "cloning $vcs"
    if ($vcs -eq "hg") {
        hg clone --verbose "ssh://hg@bitbucket.org/$repo"
    } elseif ($vcs -eq "git") {        
        git clone "git@bitbucket.org:$repo"
        if ($branch -eq $null) {
            $branch =  git rev-parse --abbrev-ref HEAD
        }
    }
    $repodir = split-path -Leaf $repo
    if ([string]::IsNullOrWhiteSpace($repodir)) { $repodir = $repo }

    pushd
    try {
        $repodir = $repodir.trimend(".hg").trimend(".git")
        cd $repodir
        if ($vcs -eq "hg") {
            hg summary
        }
        elseif ($vcs -eq "git") {
            if ($branch -ne $null) {
                write-host "updating subrepo to branch $branch"
                git checkout $branch --force
            }
            git status
        }
    } 
    finally {
        popd
    }
} 
finally {
    popd
}
