$submodules = Get-ChildItem .\submodules

foreach ($s in $submodules) {
    pushd 
    try {
        cd "$($s.fullname)"
        & .\build.ps1
    } finally {
        popd
    }
}